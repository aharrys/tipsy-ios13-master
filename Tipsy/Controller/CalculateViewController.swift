//
//  ViewController.swift
//  Tipsy
//
//  Created by Angela Yu on 09/09/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit

class CalculateViewController: UIViewController {

    @IBOutlet weak var billTotalLabel: UITextField!
    @IBOutlet weak var firstTipsButton: UIButton!
    @IBOutlet weak var secondTipsButton: UIButton!
    @IBOutlet weak var thirdTipsButton: UIButton!
    @IBOutlet weak var splitLabel: UILabel!

    var numberOfPeople =  2
    var tip = 0.10
    var totalBil = 0.0
    var resultBill = 0.0
    var finalResult = ""
    
    
    @IBAction func changeTip(_ sender: UIButton) {
        
        billTotalLabel.endEditing(true)
        firstTipsButton.isSelected = false
        secondTipsButton.isSelected = false
        thirdTipsButton.isSelected = false
        sender.isSelected = true
        
        let buttonTittle = sender.currentTitle!
        let buttontitleMinusPercent = String (buttonTittle.dropLast())
        let buttonTitleAsNumber = Double(buttontitleMinusPercent)!
        tip = buttonTitleAsNumber / 100
    }
    

    @IBAction func splitStepper(_ sender: UIStepper) {
        splitLabel.text = String(format: "%.0f",sender.value)
        numberOfPeople = Int(sender.value)
    }
    
    @IBAction func calculateButton(_ sender: UIButton) {
        let bill = billTotalLabel.text!
        if bill != "" {
            totalBil = Double(bill)!
            let result = totalBil * (1+tip) / Double (numberOfPeople)
            finalResult = String (format: "%.2f", result)
            performSegue(withIdentifier: "gotoResult", sender: self)
            
        }    
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoResult" {
            let destinationVC = segue.destination as! ResultViewController
            destinationVC.person = numberOfPeople
            destinationVC.tips = Int (tip*100)
            destinationVC.total = finalResult
        }
    }
    
}

