//
//  ResultViewController.swift
//  Tipsy
//
//  Created by Akhmad Harry Susanto on 01/05/20.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {

    @IBOutlet weak var totalString: UILabel!
    @IBOutlet weak var adviceLable: UILabel!
    
    var total = ""
    var person = 2
    var tips = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()

        totalString.text = total
        adviceLable.text = "Split between \(person) people, with \(tips)% tip."
    }
    
    @IBAction func recalculateSender(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }


}
